# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from powerdns.models import Record

class UserRecord(models.Model):
    record = models.ForeignKey(Record, unique=True)
    user = models.ForeignKey(User)
## Django blows up when we enable this :-(
#    def __unicode__(self):
#        return self.record_name

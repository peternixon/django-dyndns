# -*- coding: utf-8 -*-
from django.contrib import admin
from dyndns.models import UserRecord

class UserRecordAdmin(admin.ModelAdmin):
    list_display = ('user', 'record',)
    list_filter = ['user',]
    search_fields  = ('user', 'record',)

admin.site.register(UserRecord,UserRecordAdmin)


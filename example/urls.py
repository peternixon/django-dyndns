from django.conf.urls.defaults import *

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    (r'^nic/', include('dyndns.urls')),
    (r'^admin/', include(admin.site.urls)),
)

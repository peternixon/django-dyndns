# Welcome
## Source Distribution
Download the .zip distribution file and unpack it. From within the extracted directory run the following command:

    python setup.py install

## Tracking the Development Version
The current development version of dyndns can be checked out via Mercurial from the project site using the following command:

    hg clone http://bitbucket.org/peternixon/django-dyndns/

## Configure Your Django Settings
Add 'dyndns' to your INSTALLED_APPS setting, and disable APPEND_SLASH:

    INSTALLED_APPS = (
         # ...other installed applications,
         'dyndns',
    )

    # Required so that Django doesn't confuse DynDNS clients by redirecting to a url ending in /
    APPEND_SLASH=False

## Sync Your Database
Run the Django 'syndb' command to create the appropriate tables:

    python manage.py syncdb

## Instant Dynamic DNS Server
Add dyndns to your projects urls.py file:

    urlpatterns += patterns('',
        (r'^nic/', include('dyndns.urls')),
    )
